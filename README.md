Spawner
=======

Gather spawners with silk touch enchanted tools. Provides the ability to change mob types.

https://dev.bukkit.org/projects/spawner
https://www.spigotmc.org/resources/spawner.38624/

README
======
https://github.com/ty2u/spawner/blob/master/src/main/resources/README.txt
